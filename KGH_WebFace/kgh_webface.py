from flask import Blueprint, render_template

wc = Blueprint('index', __name__, static_folder='static_wf', template_folder='templates_wf')

@wc.route("/")
def page_start():
    return render_template('home.html')


@wc.route("/info")
def info_page():
    return render_template('info_page.html')




