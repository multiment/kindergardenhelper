from flask import Flask, render_template, url_for, redirect
from KGH_WebFace.kgh_webface import wc
from server_for_all import config_flask

app = Flask(__name__)
app.config.from_pyfile('config_flask.py')

app.register_blueprint(wc)

#@app.route("/")
def start_page():
    return 'First page' 